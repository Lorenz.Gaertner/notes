# Form factors (FFs)
## Overview Refs
  * Connection of form factors and matrix elements [3]
  * Standard papaer for z-expansion of FFs: [4]
  * for B->K, there is now a full Lattice computation of the form-factors [5]
  * small discussion on B->Knunu within SM [6]

## Introduction
The first step in computing decay odes consists in factorizing the short-distance (perturbative) and the long-distance (non-perturbative) contributions using an effective field theory. While the short-distance contributions — encoded in the Wilson coefficients — are well known and have been calculated in the literature with high accuracy, the long-distance contributions — encoded in the hadronic matrix elements — are the main obstacle to obtaining precise predictions for the observables of interest. These matrix elements can be classified into local and non-local matrix elements that are usually decomposed in terms of local and non-local FFs, respectively [1]. 

## Definition
Form factors (FFs) are scalar-valued functions, which parametrize hadronic matrix elements of exclusive semileptonic decay processes [1]. FFs are functions of the momentum transfer $q^2$. The form factors in $B \to P (=\pi, K, \bar{D})$ are $f_+^{B\to P}(q^2), f_0,^{B\to P}(q^2), f_T^{B\to P}(q^2)$ [3], 
$$
\bra{P(k)}{\bar{q}_1\gamma^\mu b}\ket{B{p}} = \left[ (p+k)^\mu - \frac{m_B^2 - m_P^2}{q^2} q^\mu \right] f_+^{B\to P} +   \frac{m_B^2 - m_P^2}{q^2}q^\mu   f_0^{B\to P}
$$
$$
\bra{P(k)}{\bar{q}_1 \sigma^{\mu\nu} \gamma_\nu b}\ket{B{p}} = \frac{i f_T^{B\to P}}{m_B+m_P} \left[q^2 (p+k)^\mu - (m_B^2 - m_P^2)q^\mu\right].$$

### Local vs non-local FFs

## Computation
The matrix elements are in general non-perturbatve quatities. 

The theory determination of the full set of local FFs is now performed by simultaneously fitting a parametrization based on the z-expansion to the LCSR determinations at low $q^2$ and the LQCD determinations at large $q^2$. This procedure provides a very accurate (and systematic) determination of the q2 dependence in the whole semileptonic region [1].

The modern method to compute form factors (FFs) [3]:
  * Lattice QCT (LQCD) for high $q^2$:
    * discretized spacetime as a UV regulator
  * Light cone sum rules (LCSR) for low $q^2$
    * 

in principle, one could
extrapolate the LQCD results at high-$q^2$ to the low-$q^2$ region. However, this would introduce a systematic error that cannot be controlled, since dispersive bounds for the local FFs in $b \to s$ transitions have so far not been very constraining. For this reason we use LCSR results, which are valid only at low $q^2$, to anchor the FFs at both ends of the phase space [1].

Combine LQCD and LCSR results by fitting FF $z$-expansion [1]
$$
\mathcal{F}_{(T), \lambda}^{B \rightarrow M}\left(q^{2}\right)=\frac{1}{1-\frac{q^{2}}{m_{J P}^{2}}} \sum_{k=0}^{\infty} \alpha_{k}^{\mathcal{F}}\left[z\left(q^{2}\right)-z(0)\right]^{k}
$$
where $m_{JP}$ denotes the mass of sub-threshold resonances compatible with the quantum nubmers of the form factor $\mathcal{F}$.





[1]: https://arxiv.org/pdf/2206.03797.pdf
[2]: https://arxiv.org/abs/1503.05534
[3]: https://link.springer.com/content/pdf/10.1007/JHEP01(2019)150.pdf
[4]: https://arxiv.org/pdf/1503.05534.pdf
[5]: https://arxiv.org/pdf/2207.12468.pdf
[6]: https://arxiv.org/pdf/2207.13371.pdf