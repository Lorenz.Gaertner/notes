# Search for $B^{+} \rightarrow K^{+} \nu \bar{\nu}$ Decays Using an Inclusive Tagging Method at Belle II
https://journals.aps.org/prl/pdf/10.1103/PhysRevLett.127.181802
https://docs.belle2.org/record/2003/files/BELLE2-NOTE-PH-2020-057.pdf

#paper #summary #inclusivetagging

## Summary
In summary, a search for the rare decay $B^{+} \rightarrow K^{+} \nu \bar{\nu}$ is performed using an inclusive tagging approach, which has not previously been used to study this process. This analysis uses data corresponding to an integrated luminosity of $63 \mathrm{fb}^{-1}$ collected at the $\Upsilon(4 S)$ resonance by the Belle II detector, as well as an off-resonance sample corresponding to $9 \mathrm{fb}^{-1}$. No statistically significant signal is observed and an upper limit on the branching fraction of $4.1 \times 10^{-5}$ at the $90 \%$ C.L. is set, assuming a SM signal. This measurement is competitive with previous results for similar integrated luminosities, demonstrating the capability of the inclusive tagging approach, which is widely applicable and expands the future physics reach of Belle II.

## Notes
* The branching fraction of the $B^{+} \rightarrow K^{+} \nu \bar{\nu}$ decay, which involves a $b \rightarrow s \nu \bar{\nu}$ transition, is predicted to be $(4.6 \pm 0.5) \times 10^{-6}$, where the main contribution to the uncertainty arises from the $B^{+} \rightarrow K^{+}$transition form factor.
* ![feynman_btosnunu.png](feynman_btosnunu.png)
* The study of the $B^{+} \rightarrow K^{+} \nu \bar{\nu}$ decay is experimentally challenging as the final state contains two neutrinos, which leave no signature in the detector and cannot be used to derive information about the signal $B$ meson.
### Inclusive tagging 
* In this case we assign the $K^+$ to the $B^+$ as the signal and everything else as _tag side_, in this case assigned to the $B^+$. 
    * **Signal**: single charged particle trajectory (track) from the kaon
    * **Tag**: remaining tracks and energy deposits (ROE - Rest Of Event) associated with the decay of the accompanying $B$.
* + : larger signal efficiency of ~4%
* -  : higher background levels
* The method exploits the distinctive topological and kinematic features of the $B^{+} \rightarrow$ $K^{+} \nu \bar{\nu}$ decay that distinguish this process from the **seven dominant background categories**:
    * other decays of charged $B$ mesons
    * decays of neutral $B$ mesons
    * five continuum categories $e^{+} e^{-} \rightarrow q \bar{q}$ with $q=u, d, s, c$ quarks and $e^{+} e^{-} \rightarrow \tau^{+} \tau^{-}$
### Cuts
* see [NOTE](https://docs.belle2.org/record/2003/files/BELLE2-NOTE-PH-2020-057.pdf)
### Classifyer
* $BDT_1$ & $BDT_2$
    * A first binary classifier $BDT_1$ is trained on approximately $10^6$ simulated events of each of the seven considered background categories and on the same number of signal events.
    * a second classifier $BDT_2$ is trained with the same set of input variables as $BDT_1$ on events with $BDT_1$ > 0.9, which corresponds to a signal efficiency of 28% and a purity of 0.02%.
    * An increase of 35% in signal purity is achieved by the additional application of $BDT_2$ on top of $BDT_1$, when comparing the performance at a signal efficiency of 4%.
    * Signal Region (SR): $BDT_1>.9$ and $BDT_2>.95$
    * Control Regions (CR):
        * CR1: $BDT_2 \in [.93, .95]$
        * CR2:  $BDT_1>.9$ and $BDT_2>.95$, off-resonance data
        * CR3: $BDT_2 \in [.93, .95]$, off-resonance data
* The decay $B^{+} \rightarrow K^{+} J / \psi$ with $J / \psi \rightarrow \mu^{+} \mu^{-}$is used as an independent validation channel, exploiting its large branching fraction and distinctive experimental signature.
    * $|\Delta E| < 100MeV$
    * $M_{bc} > 5.25 GeV$
### Statistical Analysis
* The statistical analysis to determine the signal yields is performed with the _PYHF_ package, which constructs a binned likelihood following the _HISTFACTORY_ formalism. 
* The templates for the yields of the signal and the seven background categories are derived from simulation. 
* The likelihood function is a product of Poisson probability density functions that combine the information from all 24 signal- and control-region bins defined on the on- and off-resonance data. 
* The parameter of interest, the signal strength $\mu$, is defined as a factor relative to the SM expectation and is determined simultaneously with the nuisance parameters using a simultaneous maximum-likelihood fit to the binned distribution of data event counts.
### Results
* The signal purity is found to be $6 \%$ in the SR and is as high as $22 \%$ in the three bins with $\mathrm{BDT}_{2}>0.99$. Continuum events make up $59 \%$ of the background in the SR and $28 \%$ of the events with $\mathrm{BDT}_{2}>0.99$.
* The signal strength is determined by the fit to be $\mu=4.2_{-3.2}^{+3.4}=4.2_{-2.8}^{+2.9}(\mathrm{stat})_{-1.6}^{+1.8}($ syst $)$, where the statistical uncertainty is estimated using pseudo-experiments based on Poisson statistics.
![yields.png](yields.png)
* The uncertainty on the branching fraction is used to define a measure to compare the performance of the individual tagging techniques. Assuming that this uncertainty scales as the inverse square root of the integrated luminosity [46], the inclusive approach is more than a factor of $3.5$ better per integrated luminosity than the hadronic tagging of Ref. [16], approximately $20 \%$ better than the semileptonic tagging of Ref. [19], and approximately $10 \%$ better than the combined hadronic and semileptonic tagging of Ref. [17].
## Questions
* Where does it make sense to start from if I want to redo the analysis? Can I get the selected data after the BDT classifiers and perform only the statistical analysis?
    * I can get the analysis code: https://stash.desy.de/projects/MYS/repos/b2hnn/browse?at=refs%2Ftags%2Fversion-24

* What are PID requirements?
    * Particle ID requirements
* What is signal strength $\mu$?
    * observed / SM expectation
* bias in $\mu$?
    * The bias of a parameter is the average value of its deviation from the true value
* p value for the data and fit model compatibility?
* Why can we assume that the uncertainty scales as the inverse square root of the integrated luminosity?
    * $\int L dt \propto N$

### Extensions for BSM
* The analysis now is performed assuming a SM process.
* Where does the SM play a role?
    * for $B \to K X$ the momentum spectrum of the $K$ changes, but we still only detect a $K + E_{missing}$
* What parts of the analysis would have to change for analysing wrt. BSM?
    * The BDTs are now optimised for SM data. This would not be the case anymore for data from another underlying model. Is the change so significant that we have to retrain or can we just live with the fact that we are using a slightly less than optimal BDT?

### Meeting Summary 6/7/22
Sally, Sasha, Thomas
* HEPData submission in progress
* For reinterpretation, **Selection Efficiency vs. $q^2= (M_{inv}^{\nu \bar{\nu}})^2$** most relevant
* There is no standard input for Likelihood $L$
* $L$: Resurces/HistFactory.json – all you need to do statistical analysis
    * HistFactory.json fed into _PYHF_ (need to specify release and code example)
* 2D Efficiency ?
* BDT depends on $K$ kinematics
    * want BTD in $p_T$ bins
* BSM: Dark Matter, **Wilson coeffs.**
* **TODO**
    * Get: HistFactory.json, signal predictions, SM predictions 
    * Reproduce plots only with HEPData inputs
    * Produce signal predictions