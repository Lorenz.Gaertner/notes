# Belle 2 Summer Workshop
[Indico page](https://indico.belle2.org/event/8841/)
[Documentation](https://docs.google.com/document/d/13hVf5Cxa-82jnjsiFm-C3c6mL353EzWn6lvMSchRD1Y/edit)

## Workshop structure
* 3h session: 45min lecture, 45+60min hands on session

### Outline
#### Lecture


## Open questions
* What is the knowledge level of the attendees?
* Where should the focus lie?
  * HistFactory model
  * Systematics / modifiers
  * Advanced features (hypothesis testing / limits / ...)
* Could do a $K \nu \nu$ example:
  * Give the input json (or a simplified version) and let the students reproduce the upper limit? 
* Cabinetry: can be useful for advanced features, but also for things like ntuples -> pyhf model. But this is probably a bit of an overkill.