# Sustainability in HEP

[Workshop: Sustainability in the Digital Transformation of Basic Research on Universe & Matter](https://indico.desy.de/event/37480/)
[paper](https://arxiv.org/pdf/2306.02837.pdf)

https://confluence.desy.de/display/BI/Computing+Steering+Group

https://docs.belle2.org/record/3411/files/BELLE2-NOTE-TE-2023-003.pdf

https://docs.belle2.org/record/3412/files/BELLE2-NOTE-TE-2023-004.pdf

https://accounting-next.egi.eu/wlcg/countries/normelap_processors/SITE/DATE/2022/4/2023/3/custom-belle/onlyinfrajobs/

## Travel
* CO2 compensation scheme for travelling? Maybe have recommended providers and funding for this. 
* Time-zone friendly meetings
  
## Computing
* Should we monitor the energy used by our computing resources?
* What is the cost of data transfer? Can we centralize this even more, simplify access? [article](https://stanfordmag.org/contents/carbon-and-the-cloud)
* What are software solutions that would help?
* Set MC priorities?
  
## Food
* Default veggie option for all meetings?
* Minimise buffet waste (reusable dishes)?
* 