# How to run EOS on gitpod

* connect gitpod to github
* update permissions


# How to run docker image
* pull image from https://hub.docker.com/r/eoshep/ubuntu-jammy
* `docker run -it -v $(pwd):/eos docker.io/eoshep/ubuntu-jammy`
  * `-v` bind mount local folder
  * `-it` start a Bash shell in a Docker container
* find name of precess with `docker ps -a`
* `docker exec -it *NAME* /bin/bash`
  * In order to execute commands on running containers, you have to execute “docker exec” and specify the container name (or ID) as well as the command to be executed on this container.
  * `$ docker exec <options> <container> <command>`
* exit container with `exit`
* restart container with `restart` (eg. after terminating with `Ctrl-d`)
