# Useful stuff
## Install from .AppImage
* Download `example.AppImage`
* `chmod +x example.Appimage`
* `mv example.AppImage ~/.local/bin/example`
* Then you can open the app from the terminal with `example`