# Meeting Summaries
#TODO
## Sally, Sasha, Thomas 6/7/22
Sally, Sasha, Thomas
* HEPData submission in progress
* For reinterpretation, **Selection Efficiency vs. $q^2= (M_{inv}^{\nu \bar{\nu}})^2$** most relevant
* There is no standard input for Likelihood $L$
* $L$: Resurces/HistFactory.json – all you need to do statistical analysis
    * HistFactory.json fed into _PYHF_ (need to specify release and code example)
* 2D Efficiency ?
* Normalization factor: specify model – SM (in terms of form factors)
* BDT depends on $K$ kinematics
    * want BTD in $p_T$ bins
* How to do reweighting from SM?
* BSM: Dark Matter, **Wilson coeffs.**
* **TODO**
    * Get: HistFactory.json, signal predictions, SM predictions 
    * Reproduce plots only with HEPData inputs
    * Produce signal predictions

## Group meeting 14/7/22
* EOS
    * How does the model effect the BDT output?
    * **TODO**: 
        1. $\mathcal{B}(B \to K \nu \bar{\nu})$ as a function of Wilson parameters (predicted)
        2. $p_K$ as a function of Wilson parameters
        3. BDT output as a function of Wilson parameters
        * Constrain Wilson parameters
* `HistFactory.json`
    * Nikolai offered to help/provide contact

## EOS Developer meeting 19/07/22
* Relevant material
  * `b-decays/b-to-(psd/vec)-nu-nu.(cc/hh)`
  * wilson coeff.: `wc`
  * Ref: [arxiv:2111.04327](https://arxiv.org/pdf/2111.04327.pdf)
  * Observables: `ovservables.cc`
* **TODO**:
  * Plot $\mathcal{B}(C_i)$
  * Is $p_K$ fixed by $q^2$?

## Group Meeting 21/07/22
* Showed slides `belle_gm_21-07-22`
* **TODO**
  * why is WET and SM prediction so far off?
  * impact of lepton flavor violation on theory / EOS results

## EOS 22/07/22
* Refs for form factors:
  * https://arxiv.org/pdf/2206.03797.pdf  
  * https://arxiv.org/abs/1503.05534
  * + slides from Danny
* Formfactors to use
  * B->K::f_0+f_++f_T@FLAG:2021A
  * B->K::FormFactors[f_+,f_T]@KR:2017A
  * B->K^*::FormFactors[A_0,A_1,A_2,V,T_1,T_2,T_23]@GKvD:2018A  
  * B->K^*::V+A_0+A_1+A_12@HLMW:2015A 
* Priors:
  * table 9 in [papaer](https://arxiv.org/pdf/2206.03797.pdf): parameter values for $B\to K$
  * table 10 in  [paper](https://arxiv.org/pdf/2206.03797.pdf): parameter values for $B\to K^*$
  * put $\pm 5 \sigma$ on the parameter contraints
  * check parameter distribution and plot gaussians over them
* send Meril the code on WET/SM comparison
* implement full eq.8 of (paper)[https://arxiv.org/pdf/2111.04327.pdf] in `b-to-psd-nu-nu.cc`

## EOS 29/07/22
* The discrepancy in the WET and SM came from the outdated default SM parameters for $B \to K$, this was not an issue in the $B\to K^*$ since the parameters there are up to date
* The solution is to include the SM in the analysis and use the same FF parameters
* For the implementation of the full BSM model, 
  * need to `grep SBNuNu` and maks changes
  * add more Wilson coeffs (`models/wet.hh`)
* Ideas for extra documentation?
  
## Analysis Thomas 10/08/22
* find $\epsilon(q^2)$ from analyis
* find $BDT(q^2)$ from analysis
  * maybe ask Sally
* look at MC generation and reweighting
  * "Additional weights are applied on the signal events to correct the phase space of the decay products as the MC was generated assuming on a three-body decay of the signal B"
  * Can I just apply a different reweigthing?
  * MST data: `b2--file-metadata-show steering file`

## HEPData & Analysis w/ Sally 29/08/22
* HEPData:
  * Files?
  * Which plots to reproduce?
    * most important: 'selection efficiency'
  * Scripts or resferences for plotting (PYHF)?

* Analysis code:
  * SM FF reweighting
    * Where/ How are the weights introduced in the analysis code? (Fig. 117)[https://docs.belle2.org/record/2003/files/BELLE2-NOTE-PH-2020-057.pdf]
    * If I change the weights for some BSM, how different do you expect the results to be?
      * Seen a 10-20% increase in sensitivity from no reweighting to SM reweighting
    * How SM dependent is the MC trianing data?
      * Negligible, only care about weights (phase space model).

---
* **What is the change in the kinematic structure as function of $q^2$?**
* 3 classes of signal search:
  * **DM**: resonant search (dark scalar/ALP)
  * **leptoquarks**: No **q^2** distribution change, but $\mathcal{B}$ changes
  * Axion-like: change angular observables
* IMPORTANT: tracking momentum/efficiency -> sysytematics in (code)[https://stash.desy.de/projects/MYS/repos/b2hadronnunubar_with_inclusive_tagging/browse/scripts/reconstruction]
* How does the signicicance change with the new $q^2$ distribution?
  * Change in analyisis: new weights from NP distribution
  *to see the performance with hte original analysis:
    * only update the weights and DO NOT train
* HEPData:
  * "selection efficiency" for comparison across models (take `.json` from HEPData)
  * "plot_significance"
    * update $q^2$ -> run BDTs -> apply cuts from analysis -> nr. of signal events (for $63fb^{-1}$) -> efficiency

## EOS w/ Meril 30/08/22
* PR: Scalar/Vector operators only exist if we have Majorana neutrino fields (RH neutrinos -> mass term -> Seesaw mechanism.)
* Weighting:
  * low q2: BDT lost in BG. lower weight of low q2 values helps BDT to focus on higher values
* Test sensitivity of new WC:
  * WC benchmark points:
    * separate study according to $\nu_R$ existance

## EWP meeting 06/09/22
* (Indico)[https://indico.belle2.org/event/7669/]
* Knunu update
  * MC15 - no more FF reweighting. this is already in the MC here
  * BDT1 training with MC15

## GM 08/09/22
* Check MC data for weighting
* include statistical uncertainty of MC counts $\sigma = \sqrt{N}$

## GM 22/09/22
* 2 approaches for reweighting MC:
  * unweight existing MC -> phase space
  * generate phase space signal MC (approx. 10000 events)

## GM 06/10/22
* overlay the significance of different retrainings

## Sally Meeting 11/10/22
* produce new MC samples with new kinematic distribution - WRONG

## GM 12/10/22
* BDT training did not encorporate weights
* Add weights to BDT trainig and retrain

## Thomas meeting
* efficiency (q2) is calculatable and should be easy to reinterpret within new physics
* what is not as trivial is the yield - for this we need the BDT2(q2) and do a new fitting
* need to aask sally for the ntuple of their analysis

## JSON & Fitting meeting w/ Sally 28/10/22
* json inputs for json_to_pyhf
    * json_SR: --- Y4S MC: 6 batches ---
    * json_offres: --- continuum MC --- (no signal, no BBbar - continuum MC)
* check cut ´hlt_hadron´ in note , is it needed? check cuts in general
* json_to_pyhf: binning2D option: BDT2 x pTK
* Fitting: 
    * fit_B2Knunu.py
    * json_template is the output of json_to_pyhf
    * uncertainties when fitting: When uncertainties are 0 (BBbar -> tau taubar), set them to 1e-6
* systematic uncertainties on ff (all others change only very slightly - can ignore for now)
    * in ff.csv , reflects the uncertainties in the ff. c* are the eigenvectors. (what exactly are the numbers?)
    * probably need to transform the eigenvectors between the different ff parametrizations
    * only applied to signal region (signal_*)
    * there are 12 bins in BDT2xpT
    * NOTE App F: ff error decomposition
    * FF.ipynb
    * correlation/covariance matrix is needed -> get eigenvectors
    * How to exactly produce the ff.csv from the notebook - need to check and automatize

## Fitting and reweighting w/ Sally 18/11/22
* The json weights that flow into `code/plotting/scripts/save_json_parallel.py' are just for reiweighting the relative number of MC
* Problem with minuit convergence:
  * Minuit has stability issues
  * In the analysis Minuit was used for toy studies only
  * Scipy was used for the results
  * The problem was, that for scipy no error matrix was available through pyhf (now possible?)
* CLs plot:
  * Replot with 100pts and see if discrepancy in the expectation disapears
* Yield plot:
  * plot_SRs.py
  * need new yields for that: plot_B2Knunu_yields.py (new repo) -- get_mc_counts -- my_observed_counts => jsonize
* FF systematics:
  * first approximation: treat parameters as unconstrained (uncorrelated?)
  * other option: aux data + constraint in pyhf
* unweighting/reweighting
  * add q2 bins to the variables
  * need to implement 3D variable
  * apply FF to get new 2D variable
  * do the fitting

## Systematics w/ Meril
* New FF params - play around with bounds
  * +/- 15 sigma and then readjust
* Systematics
  * Splining is problematic since you lose the information of correlation accross q2 bins
  * New method:
    * Compute the weights per q2 bin on sample level
    * Give to pyhf the mean weight, std + CORRELATION somehow

## Sally 02/12/22
* BKG only hypothisis?
  * why two files?
    * BKG only: basically no signal
    * BKG+SIG
    * one needs two becasue the fitting is done to only BKG or BKG+SIG
  * how to combine?
    * script - TBD when needed
* How to best check the yields with HEPDATA?
  * get yields from script
  * https://stash.desy.de/projects/B2D/repos/b2knunu_inclusive_2020/browse/figures/aux
  * Which file do I use?
    * BKG*SIG json
* show sally results of 3dhist and difference
  * when combining, the 1e-6 hack might be problematic?
    * do in the end
* show sally plan of syst for FF
  * how to implement this in pyhf?
    * one uncertainty with histosys

## Sally 16/12/22
* Show the analytic phase space FF
* Show the q2 distribution in each bin
* Show the differences for bins
* How to correctly calculate the statistical uncertainty?
  * calculation correct
  * try to compare individual components
  * check note on the splitting of the uncertainties (from MC counts and stat_err)
* Discuss the difference as a function of pT
  * Lower statistics
* How to use the selection efficiency?
  * assume no background
  * then taske ratio

* next year go to CERN reinterpretation workshop
* pyhf:
  * statistical uncertainty of data already in likelihood
  * statistical error in json due to MC statistics
  
## ODSL treatment of correlated uncertainties w/ Lukas 
* So apparently the feature to have correlated uncertainties is not there yet, but he will send me an example on how we could do it. He said we will need a function that takes the Wilson coefficients and the ff parameters (the alphas) as input and gives us the weights.
  ```
  def compute_weights(WC, alphas):
    p = eos.Parameters.Defaults()
    p["B->K::alpha^f+_0@BSZ2015"] = alpha[0]
    p[...] = ...
    p["sbnunu::Re{cVL}"] = WC[0]
    p[...] = ...

    BRs = [eos.Observables.make("B->Knunu::q2", eos.Kinematics(q2_min=q2_min, q2_max=q2_max), p, eos.Options(...)) for (q2_min, q2_max) in ...]
    PHSP = [phase_space_BR(q2_min, q2_max) for (q2_min, q2_max) in ...]

    return BRs/PHSP
  ```
  * Origins mailing list
  * Origins data science days in JAN
  * Help with Belle2 pyhf workshop
  

## GM 22/12/22
* What to do with the reweighted results?
  * Test for max likelihood as a function of WCs (2d projections)
  * what is the best fit?
  * fit the WC directly somehow
  * check out clusterking

## Sally 04/01/23
* FF bug: Email Sasha
* Change plot labels on parameter scans cVL -> cVLNP
* NNL < 0 issue
  * Sally checks
* Yield differences
  * Sally checks
* Check that we get same best fit values, NLL with new pyhf version
  * new version uses newer backends...
* Investigate models:
  * focus on a small number of new models
    * Leptoquarks (lepton flavour universal)
    * heavy Z'
    * light Z'
  * https://journals.aps.org/prd/pdf/10.1103/PhysRevD.105.115028
    * Look at formula changes / basis changes 
* Change normalizations on weight computation to be sensitive to cV contributions
* Check the 10% differernces they find in analysis (FF.ipynb)
* Profiling (fig 95 in note)
  * Minuit only can deliver best fit uncertainties
  * Not so important because we can only get confidence limits and there uncertainties only come in to second order
* EWP presentation
  * Focus on bigger picture
  * Talk about which models we want to target. where b-s anomalies are still strong

## Sally 20/01/23
* NNL < 0 issue
  * not an issue ?!
  * probably not
* Yield difference?
  * sally checke
* Reason for using scipy over minuit in analysis?
  * minuit:
    * CLs plot: get wiggles
    * fitting slightly different
  * scipy
    * much smoother
    * can get errors, but not in pyhf
  * correlations between scipy and minuit best fit parameters gave a straight line
  * send sally github conversation on this
* focus on observed limit, expected is not good for reinterpretation (?)
* EWP presentation
  * get feedback from sally
  * schedule presentation for next Tue
  * mention energy scales
  * link EOS/Flavio
  * Decay width: list decays for which it is valid
* Chat about theory models
  * Statistical checks to perform?
  * what affects the tensor component? Tale of invisability
  * LQs
    * what models are still useful after R(K)?
    * b->snunu constraints on models?
* Reason for old FFs in Knunu analysis?
  * new FFs should be well established
* Qualification task
  * HEPData
    * converters of data formats etc
  * NP models with basf2
    * MCHammer similar
    * EVTGen

## GM 26/01/23
* multiply BR by expectation of each model
* meet with danny/meril on possible models to study
* dicuss with thomas/nikolai on pussible WC bounds
* service task:
  * similar project already exists? ask florian
  * check out MCHammer - reweighting tool ?!
  * also PUNCH possible
* check out compute4punch
  * usable infrastructure to do fitting?
  * how would i use this?
  * what needs to be simplified?


## Meril 30/01/23
* Negative cVR - yes!
* Bounds on WCs
  * take bounds from https://arxiv.org/pdf/2111.04327.pdf and multiply by 2
* What is the most useful result?
  * bounds on WCs
* What model should I study
  * don't focus too much on specific models - most will change now due to R(K)
  * best models to look at: Z'
    * strong constraint from B->Knunu
    * small couplings as there are strong bounds from lepton decays, quark transitions (b sbar mixing) and Z-> mu nu measurements for which predictions would be modified
    * ask on discord for references
      * https://arxiv.org/pdf/0801.1345.pdf
    * The thing is, Z' model are more a ATLAS/CMS business because it just doesn't work for flavor. So most papers would by US people I think
  * U(1) lepotoquarks 

## Danny 15/02/23
* Specify the labels "C_VL+C_VR" etc
* plot actual shape modifications 
* simple example of one parameter shape change (with same integrated normalization) and the corresponding likelihood
* produce corner plots instead of surfaces in WC space

## Sally 24/02/23
* write to tale of invisability people once confident with results
* do a fit with all nuisance parameters fixed to best fit with nominal, except for WCs
  * check if uncertainty decreases (expected due to possible correlation with other modifiers)
* goodness of fit test
  * chi2 test (fast): 
    * treat post fit bin yields and datapoints as if it were a continuous fit and perform a chi2 test
  * chi2 test (toy based): 
    * many toys with small variations fitted
    * if the p value is good (subjective):
      * good data compatability
      * good fit
* qualification task: arange meeting with Thomas

## Danny 13/03/23

* try signal PDF functionality (unnormalized PDF, compute normalization once only)
* for one WC, FF point calculate integrated BR
* calculate numerator (pypmc sampling from numerator)
* use existing analysis
* TODO
  * https://github.com/eos/eos/blob/master/eos/signal-pdf.cc impement knunu (+header)
  * https://eos.github.io/doc/simulation.html

## Meril+Danny 14/03/23

* My approach so far is mixing frequentist and bayesian statistics in not the most efficient way. 
  
So far I have been:

1. Sampling from FF parameter space (8 parameters) for fixed WC, which gives me
  * samples of the differential BR in q2 bins for the sampled FF parameters depending on priors in EOS
2. To calculate the weights for reweighting the signal MC
  * I take the mean of the differential BR over the samples
  * I take the ratio of the mean to the integrated BR for the SM case. (I use this instead of the signal PDF to not neglect the overall scale that this model would predict.)
  * Then I normalize the above ratio by the phase space PDF, which is the form that the MC was generated for.
3. Apply the weights to the signal MC to obtain a signal expectation in the binning of the analysis for this set of previously fixed WCs 
4. From the sampled parameters I calculate the theory systematics
  * From the covariance matrix of the FF parameters I perform singular value decomposition to get 8 orthogonal eigenvectors.
  * I use the mean FF parameters + the individual eigenvector shifts to calculate the variations I would expect in my analysis bins.
5. To then get an exclusion limit contour I
  * Perform a hypothesis (CLs) test with the expected signal according to the weights and 8 nuisance parameters corresponding to the systematic variations.
  * This gives me an observed CLs for a particular point in WC space.
  * I then do this in a grid of WC space, to get an exclusion limit (CLs<0.05).

This issue I tried to solve:
  * I wanted to automatize points 1.-4., such that I can do a ML fit with unconstrained WCs. So basically, the fit can vary the WCs, steps 1-4 are performed in the background return what the corresponding modification and its uncertainties would be.

The method that Meril suggested:
1. Sampling from FF parameter (8 parameters) + WC (effectively 3 parameters) space, which gives me
  * samples of the differential BR in q2 bins for given FF parameters and WCs
2. **For each sample**, calculate the weights for reweighting the signal MC
  * Same as above in (2.)
3. Apply the weights to the signal MC to obtain a signal expectation in the binning of the analysis for this set of WCs 
4. Perform a fit without theory uncertainties 
  * Obtain a NLL for each of the samples
5. To get an exclusion limit
  * Use the NLL as a weight of each sample
  * Plot contours in WC space

  This last step is still unclear to me. How is the dependence on the FF parameters removed? And what exactly do I plot?

  ---------------------------

  L(x | mu, sigma, eff-params(WCs), WCs) = L(x | mu, sigma, detector eff, hadr params, WCs)

eg. for one WC similar parameter
  L(x | mu, sigma, C) = Normal(x | mu = mu(C), sigma) with mu(C) = mu_0 + Exp(-|C|^2)

  P(WC, FFs | D, M) = LLH(D, M | WC, FFs) * P_0(WC) * P_0(FFs) / Z
  WC: Wilson coefficients
  FF: hadronic parameters
  M: hadronic model + BSM model(WET)
  D: belle data

  Aims:
  * Profile wrt detect eff and hadr params, but keep WCs
  * Move WCs to LHS of L, i.e. make them explicit

  1. Sample from prior of hadr params + WCs
  2. For each sample
     1. compute BR
     2. compute signal MC weights
     3. apply weights
     4. fit for NLL in pyhf (no theory uncertainties)
  3. Use NLL to weight sample from step 1
  4. 2d histogram in WC space (KDE approx for smoothing)

## DPG feedback
* buras: great, where is K* so we can do combinations. important point: q2 -- if we have K* also do not need q2 info, actually enough to have BRs. 
* do we force the use of eos?
* next time: make more clear that the likelihood becomes a function of the WCs

## GM 30/03/23
* Check how KDE works
* Why do I get different contours?
* Reinterpretation forum:
  1. Publish paper with new results
  2. Publish paper with old results
  3. Present with preliminary results
  4. Present with Poisson smeared data
* Consider publishing with existing result for reinterpretation forum
* Contact Sasha and Sally about reinterpretation forum

## Sally, Thomas 05/05/23
* Reinterpretation forum:
  * Plans (ordered to priority)
    1. Present with published results
    2. Present with Poisson smeared data (method focussed)
  * Sent around abstract
  * What are the next steps here?
  * Deadline 16 July
  * -----
  * Plan:
    * Finalize abstract
      * Send abstract to authors for comments
    * Send abstract to Elisa, Sasha, Diego
      * 'only using public data + signal MC q2'
      * 'would like to do this the official Belle II way, to give the collaboration the opportunity to add comments'
      * 'would be nice to have this as work that comes from the collaboration'
    * Send abstract to speakers committee
      * Who to add as author / how to write it exactly?
* Paper
  * [Publication checklist](https://confluence.desy.de/display/BI/Publications+Check+List)
  * Note in preparation
  * Finish first draft by next week (hopefully)
  * Plans (ordered to priority)
    1. Publish paper with new results
    2. Publish paper with old results
  * -----
  * Plan:
    * Prepare note for old analysis
    * If timeline fits, publish with old analysis and have a section in new paper with method applied
* Updates
  * New method implemented in pyhf
    * VERY neat now
    * also very fast
  * Still some issue with C^2 * FF^2 
    * Reason why minuit fails?
      * Minuit might fail because of EDM parameter setting -- Minuit implementation in pyhf is a bit strange
        * Still send error to Sally
      * Scipy should work
    * The fact that this is multiplicative should not be an issue, also because constraints are different
    * (confirm with Lukas at some point)
    * Can visualize likelihood function somehow?
      * translate to ROOWorkspace...
  * Two different exclusion methods - discussion needed on which is better
    * Data - Model compatability
    * -lnL should be chi2 distributed - check this
    * Should compare the methods and put one in the appendix
* Other things:
  * Michael Schmitt (theory author, now at KIT): happy to answer questions / get comments

## Danny 09/05/23
* To show that shape changes are important for reinterpretation we should plot:
  you could colour the WC space as follows:
 - a point that is possible at 95% CL in the old procedure but is excluded at 95% CL in our procedure will be marked green (too conservative in the old procedure)
 - a point that is excluded at 95% CL in the old procedure but is allowed at 95% CL in our procedure will be marked red (too aggressive in the old procedure)
* old procedure: Only calculating the expected BR as a function of WCs and scaling accordingly, but keeping the SM kinematic distribution
* new procedure: Taking into account the change in the kinematic distribution as a function of WCs
* Hypothesis test with
  * H1: excluded with method 1
  * H2: excluded with method 2
* Fit Gaussian mixture model to the posterior as in https://arxiv.org/pdf/2302.05268.pdf

## Danny, Nikolai, Sally 10/05/23
(https://docs.google.com/document/d/1l6XqKfGo3ZRTGJg-Le_gpxHbquPISQ0eDnoIe8NTJwA/edit?usp=sharing)
* The meeting was mainly to discuss the statistical approach to take for future work. The options were
  * a more frequentist approach, calculating CLs values for a grid in WC space
  * a more bayesian approach, sampling from WCs (flat prior) and hadronic parameters (constrained) and using likelihoods determined from fitting to obtain the posterior PDF.
* Generally, the similarity of the results is no supprise, since it should be independent (in the limit of infinite data). Danny pointed to https://arxiv.org/pdf/hep-ph/0607246.pdf
* Danny mentioned that the most useful application would be to parameterize the posterior distribution, marginalized over the hadronic parameters and profiled over the experimental parameters and publish that.
* Given this, the bayesian method seems more favourable, also since a similar study was done before (https://arxiv.org/pdf/2302.05268.pdf). A similar approach could be taken, which would speed up the process.
  * Here, the parametrization of the posterior distribution was done in terms of a Gaussian mixture model. 
  * This could be adapted, but maybe a corresponding feature is planned for the bayesian extension of pyhf (TBD with Malin and Lukas).
* To the political issue of bayesian vs. frequentist for Belle 2 publications is less of an issue, we believe. The main goal is to obtain a model independent likelihood function, which could be published in the pyhf json format. Publishing also the posterior pdf would be a useful first application, independently of the approach taken. (double check with Thomas)
* Danny also mentioned an issue with the theoretical model, due to the flavour of the undetected final state neutrinos (this is an important point, but certainly needs further discussion)
  * There can be long distance contributions such as B^+ -> tau^+(-> K^+ antinu_tau) nu_tau etc., which would modify the rate and hence need to be taken into account (https://arxiv.org/pdf/0908.1174.pdf)
  * Also the posibility of differently flavoured neutrinos could occur in BSM cases. Currently we assume the WCs to be the same, indepensently of the neutrino flavours.
  * Generally, we need to find a consistent way to itterate over processes that contribute and add them to obtain the correct kinematic distribution as a function of relevant WCs.
  * This should mostly be done within EOS. Once implemented, the modifications to the code calculating the likelihood would be minimal.

## GM 17/05/23
* Long distance contribution:
  * B_> tau nu in background MC? does this take care of the LD contribution in the analysis?
* Abstract:
  * "If no objections, will send to speakers committee"
## Sally 16/06/23
* Project
  * Focus on publication
    * Ask Thomas' opinion about 'proof of concept' paper
    * write paper draft
  * Speakers committee: Sven Vahsen sevahsen at hawaii dot edu (cc elisa, sasha)
  * Do toy study for general fit
* pyhf
  * systematics:
    * rerun reconstruction
    * toy based approach
    * get links from sally to notebooks

## EWP 27/06/23
* WG approval within 2 weeks
* present physics meeting 10/07/23
* 
* run more samples

## Sally 30/06/23
* See how to modify abstract and submit.
* Try to adress all points by sasha - maybe go with CLs, as this is easier to understand in experimental HEP
* Paper: If we do not publish as Belle II, we publish method without. 
  
* Did cross check to reproduce original limit
* Running more bayesian samples
* Small bug in plotting for CLs

* EWP 18.7.
* Physics 14.8.

* Make Sven submit abstract

* Studies
  * reproduce limit   -- done
  * random model
  * compare methods
  * Public:
    * cls
    * q2 dist
    * likelihood
    * belle2 style

## Sally 06/07/23
* differences in statistical methods
  * plot CLs_exp and all P values
  * Not a showstopper if we do not have perfect agreement
* add Belle 2 plotting style:
  * from plotting.hep_plotters import plot_b2_logo
* email sasha on approval talk in ewp after meeting next friday
