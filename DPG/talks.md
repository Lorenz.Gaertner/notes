# [Timetable](https://www.dpg-verhandlungen.de/year/2023/conference/smuk/day/2023-03-20)

# Monday
11:00   HSZ/AUDI  T 1.1 	[Invited Talk: What we learned about the Higgs Boson from the LHC so far, Duc Bao Ta](https://www.dpg-verhandlungen.de/year/2023/conference/smuk/part/t/session/1/contribution/1)

12:00   HSZ/AUDI	T 1.3 	[Invited Talk: The charm and beauty of flavour physics — •**Marco Gersabeck**](https://www.dpg-verhandlungen.de/year/2023/conference/smuk/part/t/session/1/contribution/3)

12:30 	HSZ/AUDI    PV III 	[Prize Talk: Two milestones in the life of the Universe: Last Scattering Surface and Black Body Photosphere — •Rashid Sunyaev](https://www.dpg-verhandlungen.de/year/2023/conference/smuk/part/pv/session/3/contribution/1)

16:30   ZEU/0255 	GR 2.1 	[Anisotropies in the Cosmological Gravitational Wave Background — •Florian Schulze](https://www.dpg-verhandlungen.de/year/2023/conference/smuk/part/gr/session/2/contribution/1)

16:30 	GER/038     AKE 2.1 	Invited Talk: The German primary energy consumption -- status and trends — •Larissa Breuning, Alexander von Müller, and Anđelka Kerekeš
	
17:00 	GER/038     AKE 2.2 	Das Windenergiepotenzial Deutschlands: Grenzen und Konsequenzen grossräumiger Windenergienutzung — •Axel Kleidon
	

17:00 	HSZ/0101    T 5.3 	Search for inelastic Dark Matter with a Dark Higgs at Belle II — •Patrick Ecker, Giacomo De Pietro, Jonas Eppelt, Torben Ferber, and Pablo Goldenzweig

17:15 	HSZ/0004    T 2.4 	CP violation measurement in B0 → D+ D− and Bs0 → Ds+ Ds− decays at the LHCb experiment — Johannes Albrecht, •Louis Gerken, Philipp Ibis, and Antje Mödden
	
17:30 	HSZ/0004    T 2.5 	CP violation in τ→ KS π ντ decays at Belle* — •Katarina Dugic, **Daniel Greenwald**, and Stephan Paul for the Belle II collaboration 

# Tuesday

09:45 	HSZ/AUDI + HSZ/0002 + HSZ/0003  PV V 	Plenary Talk: The European Destination Earth initiative -- a paradigm change for weather and climate prediction — •Peter Bauer

11:00 	HSZ/0401    GR 3.1 	Invited Talk: Scalaron-Higgs inflation — •Christian Steinwachs

11:00 	JAN/0027    AGPhil 2.1 	Einstein's forgotten interpretation of GR: against geometrization and for the unification of gravity and inertia — •Dennis Lehmkuhl
	
11:30 	JAN/0027    AGPhil 2.2 	A dynamical perspective on the arrow of time — •Kian Salimkhani

12:05 	HSZ/0401    GR 3.3 	Inflation and its Discontents — •Marc Holman

12:00 	HSZ/0304    MP 3.3 	Invited Talk: Emergence of gravity from conformal field theory — •Nele Callebaut

12:30 	HSZ/AUDI    PV VI 	Prize Talk: The Higgs boson at the (HL)LHC -- precisely! — •Adinda de Wit

14:00 	HSZ/AUDI    PV VII 	Ceremonial Talk: The once unattainable -- new breakthroughs in particle physics — •Monica Dunford

17:00 	HSZ/0401T   26.1 	Multi-lepton B decays within the Standard Model and their impact on LHCb analysis — Johannes Albrecht, Emmanuel Stamou, Vitalii Lisovskyi, and •Jan Peter Herdieckerhoff

17:40 	ZEU/0255    GR 5.3 	On the redshift and relativistic gravity potential determination in GR — •Dennis Philipp, Eva Hackmann, and Claus Laemmerzahl

18:15 	HSZ/0304    T 25.6 	Analysis of B→ µ ν with inclusive tagging at Belle II — Florian Bernlochner, Jochen Dingfelder, •Daniel Jacobi, Peter Lewis, and Markus Prim for the Belle II collaboration 

# Wednesday
11:00 	ZEU/0260    GR 6.1 	Invited Talk: Geodesic motion in relativistic astrophysics — •Eva Hackmann

11:00 	HSZ/AUDI    T 50.1 	Invited Topical Talk: Search for leptoquarks at the ATLAS experiment — •Mahsana Haleem for the ATLAS collaboration 

15:50 	HSZ/0304    T 54.1 	Systematic Parametrization of the B-meson Light-Cone Distribution Amplitude — Thorsten Feldmann, •Philip Lüghausen, and Danny van Dyk

16:05 	HSZ/0401    T 55.2 	Inclusive analysis of untagged B → X l+l− decays at Belle II — •Arul Prakash Sivagurunathan, Sviatoslav Bilokin, and Thomas Kuhr

16:35 	HSZ/0401    T 55.4 	Measurement of the branching fractions and differential kinematic distributions of B+/0 → X J/ψ with hadronic tagging — Florian Bernlochner, Jochen Dingfelder, Thomas Kuhr, •Martin Angelsmark, William Sutcliffe, and Sviat Bilokin for the Belle II collaboration 

16:40 	ZEU/0255    GR 10.3 	Comparison of Models of Dark Energy — •Paul Sawitzki, Jannes Ruder, and Hans-Otto Carmesin
	
17:00 	ZEU/0255    GR 10.4 	Comparison of Models of the H0 Tension — •Philipp Schöneberg, Phil Immanuel Gustke, and Hans-Otto Carmesin

17:45 	HSZ/0401    T 77.2 	Updated Search for Rare Electroweak Decay B → K(*)νν to Constrain New Physics Models — •Caspar Schmitt, Sviatoslav Bilokin, and Thomas Kuhr
	
18:00 	HSZ/0401    T 77.3 	Enhancing data exploitation with public likelihoods — •Lorenz Gaertner, Thomas Kuhr, Danny van Dyk, Lukas Heinrich, Méril Reboud, and Slavomira Stefkova

18:30 	HSZ/0304    T 78.5 	Untagged B0 → D*+ ℓ− νℓ studies with Belle II — Florian Bernlochner, Lu Cao, Jochen Dingfelder, and •Chaoyi Lyu

20:00 	HSZ/AUDI    PV XII 	Evening Talk: Max-von-Laue Lecture: Risikokompetenz – informiert und entspannt mit Risiken umgehen — •Gerd Gigerenzer

# Thursday

09:00 	HSZ/AUDI + HSZ/0002 + HSZ/0003  PV XIII 	Plenary Talk: The role of artificial intelligence in modern radiation therapy — •Guillaume Landry

09:45 	HSZ/AUDI + HSZ/0002 + HSZ/0003  PV XIV 	Plenary Talk: Machine Learning Advances in Particle Physics — •Lukas Heinrich

11:00 	HSZ/AUDI    ST 7.1 	Invited Talk: AI Techniques for Event Reconstruction — •Ivan Kisel

12:00 	HSZ/AUDI    ST 7.3 	Invited Talk: Is this even physics? -- Progress on AI in particle physics — •Gregor Kasieczka

12:30 	HSZ/AUDI    PV XV 	Prize Talk: Direct dark matter detection: What if there's no WIMP? — •Belina von Krosigk

14:00 	ZEU/0148    AKjDPG 2.1 	Adamant: A JSON-Based Metadata Editor for Researchers — •Ihda Chaerony Siffa, Marjan Stankov, and Markus M. Becker

14:00 	HSZ/0002    AGA 4.1 	Invited Talk: Mass Starvation? Impacts of Nuclear War on Climate Change and Food Security — Lili Xia and •Kim Scherrer

14:40 	HSZ/0003    T 101.3 	Invited Topical Talk: Belle II opportunities in B-decays with invisible signatures — •Slavomira Stefkova for the Belle II collaboration 

15:00 	HSZ/0003    T 101.4 	Invited Topical Talk: Two Pieces of a Puzzle: Inclusive and Exclusive |Vcb| — •Markus Prim

15:00 	HSZ/0004    T 102.4 	Invited Topical Talk: New Ideas for Baryo- and Leptogenesis — •Kai Schmitz

15:45 	HSZ/0004    T 103.1 	Efficient Sampling from Differentiable Matrix Elements with Normalizing Flows — •Annalena Kofler, Vincent Stimper, Mikhail Mikhasenko, Michael Kagan, and Lukas Heinrich

15:50 	HSZ/0401    T 105.1 	Search for the lepton flavour violating decay B0 → τ± ℓ∓ — •Nathalie Eberlein, Thomas Kuhr, and Thomas Lück

16:00 	ZEU/0148    AKjDPG 2.4 	Invited Talk: Open data and open-source tools throughout research data life cycle: KCDC example — •Victoria Tokareva

16:20 	HSZ/0304    T 104.3 	Studies of B → D** ℓ ν at Belle II — Gerald Eigen, Ariane Frey, and •Noreen Rauls
	
16:35 	HSZ/0304    T 104.4 	Untagged analysis of B → π ℓ νℓ and B → ρ ℓ νℓ and extraction of |Vub| at Belle II — Florian Bernlochner, Jochen Dingfelder, •Svenja Granderath, and Peter Lewis for the Belle II collaboration 

18:30 	HSZ/0304    T 129.5 	Probing lepton universality in inclusive semileptonic B-meson decays at Belle II — Florian Bernlochner, Jochen Dingfelder, •Henrik Junkerkalefeld, and Peter Lewis for the Belle II collaboration 

# Friday

09:00 	HSZ/AUDI + HSZ/0002 + HSZ/0003  PV XVII 	Plenary Talk: The Einstein Telescope — •Harald Lück

09:45 	HSZ/AUDI + HSZ/0002 + HSZ/0003  PV XVIII 	Plenary Talk: The LHC legacy and prospects — •Markus Klute