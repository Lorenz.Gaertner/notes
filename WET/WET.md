# Weak Effective Theory

Good overview: [2]

Good formal derivation of the effective Lagrangian: [1], p. 17-19

Lectures on EFT: [3]

[1]: https://arxiv.org/pdf/hep-ph/9512380.pdf
[2]: **/home/l/Lorenz.Gaertner/Documents/refs/Flavour_Lectures_2021.pdf**
[3]: https://arxiv.org/pdf/1903.03622.pdf
